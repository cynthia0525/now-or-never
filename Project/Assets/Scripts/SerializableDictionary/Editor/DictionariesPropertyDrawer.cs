﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(SpriteDictionary))]
[CustomPropertyDrawer(typeof(AudioDictionary))]
public class DictionariesPropertyDrawer : SerializableDictionaryPropertyDrawer { }
