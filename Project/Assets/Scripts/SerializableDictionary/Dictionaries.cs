﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpriteDictionary : SerializableDictionary<string, Sprite> { }

[System.Serializable]
public class AudioDictionary : SerializableDictionary<string, AudioClip> { }