﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : NetworkBehaviour
{
    int oneMinute = 60;
    [SyncVar(hook = "OnChangeTimeLeft")]
    int timeLeft = 0;

    [Header("[Setting]")]
    [SerializeField]
    int timeLimit = 0;

    // Start is called before the first frame update
    void Start()
    {
        // prevent misplacement of UI
        transform.localPosition = new Vector2(125, -50);

        if (isServer)
        {
            timeLeft = timeLimit * oneMinute;
            InvokeRepeating("UpdateTimer", 0f, 1f);
        }

        // synchronize UI on initialization
        OnChangeTimeLeft(timeLeft);
    }

    void UpdateTimer()
    {
        timeLeft--;

        // update UI on server
        OnChangeTimeLeft(timeLeft);

        if (timeLeft == 0)
        {
            CancelInvoke("UpdateTimer");
            PlayerController[] players = FindObjectsOfType<PlayerController>();
            foreach (PlayerController player in players)
                Destroy(player.gameObject);
            if (!isClient) BackgroundMusicController.instance.ChangeScene("Landing");
            RpcTimeIsUp();
        }
    }

    void OnChangeTimeLeft(int timeLeft)
    {
        int minute = timeLeft / oneMinute;
        int second = timeLeft % oneMinute;
        string timeString = string.Format("{0}:{1:D2}", minute, second);
        gameObject.GetComponent<Text>().text = timeString;
    }

    [ClientRpc]
    void RpcTimeIsUp()
    {
        if (SceneManager.GetActiveScene().name == "MainScene")
            BackgroundMusicController.instance.ChangeScene("GameOver");
    }

    [Server]
    public void OnDisconnect(NetworkInstanceId netId)
    {
        string targetId = netId.ToString();
        PlayerStatus playerStatus = GameObject.Find("PlayerStatus").GetComponent<PlayerStatus>();
        RpcRemovePlayerStatus(targetId);
        PlayerManager.instance.playerDictionary.Remove(netId);
        playerStatus.playerDictionary.Remove(netId);
        if (!isClient) playerStatus.RemovePlayer(targetId);
    }

    [ClientRpc]
    void RpcRemovePlayerStatus(string targetId)
    {
        GameObject.Find("PlayerStatus").GetComponent<PlayerStatus>().RemovePlayer(targetId);
    }
}
