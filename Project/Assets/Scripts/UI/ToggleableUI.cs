﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleableUI : MonoBehaviour
{
    void Awake()
    {
        ToggleActivation();
    }

    public void ToggleActivation()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
