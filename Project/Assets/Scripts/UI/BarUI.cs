﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarUI : MonoBehaviour
{
    [SerializeField]
    Image mask = null;
    float originalSize;

    void Start()
    {
        originalSize = mask.rectTransform.rect.width;
    }

    public void SetValue(float value, bool direction = true)
    {
        RectTransform.Axis axis = direction ? RectTransform.Axis.Horizontal : RectTransform.Axis.Vertical;
        mask.rectTransform.SetSizeWithCurrentAnchors(axis, originalSize * value);
    }
}

