﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHP : BarUI
{
    public static PlayerHP instance { get; private set; }

    void Awake()
    {
        instance = this;
    }

    [SerializeField]
    Transform layout;
    [SerializeField]
    GameObject weaponDurability;

    string GetWeaponIdentifier(string weapon)
    {
        return string.Format("DurabilityUI[{0}]", weapon);
    }

    public WeaponDurability AddWeapon(string weapon)
    {
        GameObject durabilityObject = Instantiate(weaponDurability);
        durabilityObject.name = GetWeaponIdentifier(weapon);
        durabilityObject.transform.SetParent(layout, false);

        WeaponDurability durabilityUI = durabilityObject.GetComponent<WeaponDurability>();
        durabilityUI.InitWeapon(weapon);
        return durabilityUI;
    }

    public WeaponDurability RemoveWeapon(string weapon)
    {
        string target = GetWeaponIdentifier(weapon);
        WeaponDurability[] durabilityUI = GetComponentsInChildren<WeaponDurability>();
        foreach (WeaponDurability ui in durabilityUI)
            if (ui.name == target) Destroy(ui.gameObject);
        return null;
    }
}
