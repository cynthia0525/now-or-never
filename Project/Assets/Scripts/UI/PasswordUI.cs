﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PasswordUI : NetworkBehaviour
{
    [SyncVar(hook = "OnChangePassword")]
    string password;

    // Start is called before the first frame update
    void Start()
    {
        if (isServer)
        {
            Text[] codeText = gameObject.GetComponentsInChildren<Text>();
            string temp = "";
            for (int i = 0; i < codeText.Length; i++)
                temp = temp + "?";
            password = temp;
        }

        // synchronize UI on initialization
        OnChangePassword(password);
    }

    [Server]
    public void SetContent(int index, char code)
    {
        char[] codes = password.ToCharArray();
        codes[index] = code;
        password = new string(codes);

        // update UI on server
        OnChangePassword(password);
    }

    void OnChangePassword(string password)
    {
        Text[] codeText = gameObject.GetComponentsInChildren<Text>();
        for (int i = 0; i < codeText.Length; i++)
            codeText[i].text = password[i].ToString();
    }
}
