﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : BarUI
{
    Transform self;
    Vector3 offset;
    Vector2 baseScale;

    void Awake()
    {
        self = transform.parent;
        offset = transform.position - self.position;
        baseScale = transform.localScale;
    }

    void LateUpdate()
    {
        transform.position = self.position + offset;
    }

    public void AvoidFlipping(Vector2 flipX)
    {
        Vector2 localScale = baseScale * flipX;
        transform.localScale = localScale;
    }
}
