﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    string current = "Menu";

    void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject subpage = transform.GetChild(i).gameObject;
            if (subpage.name == current)
                subpage.SetActive(true);
            else subpage.SetActive(false);
        }
    }

    void SwitchPage(string next)
    {
        GameObject currentSubpage = transform.Find(current).gameObject;
        GameObject nextSubpage = transform.Find(next).gameObject;
        currentSubpage.SetActive(false);
        nextSubpage.SetActive(true);
        current = next;
    }

    public void OnClick(string action)
    {
        switch (action)
        {
            case "NewGame":
                BackgroundMusicController.instance.ChangeScene("ItemSelection");
                break;
            case "HowToPlay":
            case "KeyGuide":
            case "About":
            case "Menu":
                SwitchPage(action);
                break;
            default:
                break;
        }
    }
}
