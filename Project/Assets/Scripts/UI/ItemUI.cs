﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ItemUI : MonoBehaviour
{
    [SerializeField]
    public SpriteDictionary propDictionary;
    public Canvas mapPopup;
    public Canvas itemKeys;
    static List<string> itemList = new List<string> { };
    public OneOffItem[] items;

    public void ConfirmSelection()
    {
        InputField playerName = GetComponentInChildren<InputField>();
        if (string.IsNullOrEmpty(playerName.text))
        {
            ErrorMessage.instance.ThrowError("Player name cannot be empty");
            return;
        }
        PlayerManager.instance.InitPlayerName(playerName.text);
        Dropdown[] dropdowns = GetComponentsInChildren<Dropdown>();
        foreach (Dropdown dropdown in dropdowns)
            itemList.Add(dropdown.options[dropdown.value].text);
        BackgroundMusicController.instance.ChangeScene("Lobby", 1);
    }

    void Awake()
    {
        string scene = SceneManager.GetActiveScene().name;
        switch (scene)
        {
            case "ItemSelection":
                break;
            case "MainScene":
                InitItems();
                break;
            default: break;
        }
    }

    void InitItems()
    {
        items = GetComponentsInChildren<OneOffItem>();
        for (int i = 0; i < items.Length; i++)
        {
            string item = itemList[i];
            items[i].SetItem(item, propDictionary[item]);
        }
    }
}
