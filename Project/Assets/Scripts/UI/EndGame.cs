﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    public void Action(bool isContinue)
    {
        float duration = 0.5f;
        AudioSource bgm = BackgroundMusicController.instance.GetComponent<AudioSource>();
        Invoke(isContinue ? "Restart" : "Quit", duration);
        StartCoroutine(AudioController.FadeOut(bgm, duration));
    }

    void Restart()
    {
        MatchMakingManager.instance.LeaveRoom();
    }

    void Quit()
    {
        Application.Quit();
    }
}
