﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorMessage : ToggleableUI
{
    public static ErrorMessage instance { get; private set; }

    [SerializeField]
    Text description;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
        ToggleActivation();
    }

    public void ThrowError(string message)
    {
        description.text = message;
        ToggleActivation();
    }
}
