﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PasswordPopup : MonoBehaviour
{
    InputField input;
    PlayerController player;

    public void SetTarget(GameObject target)
    {
        player = target.GetComponent<PlayerController>();
    }

    public void OpenPopup()
    {
        FocusInput();
        gameObject.SetActive(true);
    }

    void ClosePopup()
    {
        gameObject.SetActive(false);
        player.blockInput = false;
    }

    void FocusInput()
    {
        EventSystem.current.SetSelectedGameObject(input.gameObject);
        input.OnPointerClick(new PointerEventData(EventSystem.current));
    }

    void HandleSumbit(string inputPassword)
    {
        if (Input.GetKeyDown(KeyCode.Escape)) ClosePopup();
        else if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
        {
            PropSetting setting = GetComponentInParent<PropSetting>();
            // secret password: development use
            List<string> passwordList = new List<string> { setting.password, "GFHG", "SDGM", "CUHK", "CSCI" };
            if (passwordList.Contains(inputPassword.ToUpper()))
            //if (inputPassword.ToUpper() == setting.password)
            {
                gameObject.SetActive(false);
                BackgroundMusicController.instance.ChangeScene("GameClear");
                player.CmdDestroy(0, "Clear");
            }
            else
            {
                input.text = "";
                FocusInput();
            }
        }
    }

    void Start()
    {
        input = GetComponentInChildren<InputField>();
        input.onEndEdit.AddListener(HandleSumbit);
        gameObject.SetActive(false);
    }
}
