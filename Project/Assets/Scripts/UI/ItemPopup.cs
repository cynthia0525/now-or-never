﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPopup : MonoBehaviour
{
    public void SetContent(SpriteDictionary propDictionary, string itemType)
    {
        GameObject item = transform.Find("Board/Grid/Item").gameObject;
        GameObject info = transform.Find("Board/Info").gameObject;
        if (string.IsNullOrEmpty(itemType))
        {
            item.GetComponent<Image>().sprite = null;
            info.GetComponent<Text>().text = "There is nothing in the box.";
        }
        else
        {
            item.GetComponent<Image>().sprite = propDictionary[itemType];
            info.GetComponent<Text>().text = string.Format("You have got a {0}.", itemType.ToLower());
        }
        gameObject.SetActive(true);
    }

    void Start()
    {
        gameObject.SetActive(false);
    }

    void FixedUpdate()
    {
        if (gameObject.activeSelf && Input.GetButton("Cancel"))
            gameObject.SetActive(false);
    }
}
