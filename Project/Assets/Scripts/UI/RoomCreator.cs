﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomCreator : MonoBehaviour
{
    [SerializeField]
    Text sliderText;

    void Start()
    {
        gameObject.SetActive(false);
    }

    public void InitInput()
    {
        InputField roomName = GetComponentInChildren<InputField>();
        roomName.text = "";
        Slider roomSize = GetComponentInChildren<Slider>();
        roomSize.value = 1;
        gameObject.SetActive(true);
    }

    public void OnSliderValueChange(float value)
    {
        sliderText.text = value.ToString("0");
    }

    public void OnCancel()
    {
        gameObject.SetActive(false);
    }

    public void OnSubmit()
    {
        InputField roomName = GetComponentInChildren<InputField>();
        if (string.IsNullOrEmpty(roomName.text)) return; // room name cannot be null or empty
        Slider roomSize = GetComponentInChildren<Slider>();
        MatchMakingManager.instance.CreateRoom(roomName.text, (uint)roomSize.value);
        gameObject.SetActive(false);
    }
}
