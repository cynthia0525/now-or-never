﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerStatus : MonoBehaviour
{
    [SerializeField]
    GameObject status;
    [SerializeField]
    public SpriteDictionary statusDictionary;
    public Dictionary<NetworkInstanceId, string> playerDictionary = new Dictionary<NetworkInstanceId, string>();

    public void AddPlayer(string id, string name, string status)
    {
        string statusIdentifier = string.Format("PlayerStatus[{0}]", id);
        if (transform.Find("Layout/" + statusIdentifier) != null)
        {
            // prevent adding existing status due to network delay
            return;
        }
        Transform layout = transform.Find("Layout");
        GameObject playerStatus = Instantiate(this.status);
        playerStatus.name = statusIdentifier;
        playerStatus.transform.SetParent(layout, false);
        playerStatus.GetComponent<Image>().sprite = statusDictionary[status];
        playerStatus.GetComponentInChildren<Text>().text = name;
    }

    public void RemovePlayer(string id)
    {
        GameObject player = transform.Find(string.Format("Layout/PlayerStatus[{0}]", id)).gameObject;
        Destroy(player);
    }

    public void ChangeStatus(string id, string status)
    {
        GameObject player = transform.Find(string.Format("Layout/PlayerStatus[{0}]", id)).gameObject;
        player.GetComponent<Image>().sprite = statusDictionary[status];
    }
}
