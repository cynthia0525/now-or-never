﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponDurability : BarUI
{
    [SerializeField]
    Image weaponIcon;
    [SerializeField]
    public SpriteDictionary weaponDictionary;

    public void InitWeapon(string weapon)
    {
        weaponIcon.sprite = weaponDictionary[weapon];
    }

    public void UpdateUI(int durability)
    {
        float maskedValue = Mathf.Max(Weapon.INIT_STR - durability, 0);
        SetValue(maskedValue / Weapon.INIT_STR, false);
    }
}
