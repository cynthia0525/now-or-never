﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PropSetting : NetworkBehaviour
{
    int numberOfPlayers = 1;

    [SerializeField]
    public SpriteDictionary propDictionary;
    Dictionary<string, char> passwordDictionary = new Dictionary<string, char>();
    [SyncVar]
    public string password;

    public Canvas itemPopup;
    public Canvas passwordUI;

    // Start is called before the first frame update
    void Start()
    {
        if (!isServer) return;

        Cube[] cubes = GetComponentsInChildren<Cube>();

        // assign the password of this match
        while (passwordDictionary.Count < 4)
        {
            int index = Random.Range(0, cubes.Length);
            if (string.IsNullOrEmpty(cubes[index].item))
            {
                cubes[index].item = "Scroll";
                char code = (char)('A' + Random.Range(0, 26));
                passwordDictionary.Add(cubes[index].name, code);
            }
        }

        // synchronize the password for matching on client
        List<char> codes = new List<char>(passwordDictionary.Values);
        password = string.Join("", codes);

        // assign items to different boxes
        List<string> propList = new List<string>(propDictionary.Keys);
        for (int i = 1; i < propList.Count; i++)
        {
            string prop = propList[i];
            int propCount = Random.Range(0, numberOfPlayers + 2);
            int current = 0;
            while (current < propCount)
            {
                int index = Random.Range(0, cubes.Length);
                if (string.IsNullOrEmpty(cubes[index].item))
                {
                    cubes[index].item = prop;
                    current++;
                }
            }
        }
    }

    [Server]
    public void GetPassword(string cubeName)
    {
        List<string> cubeList = new List<string>(passwordDictionary.Keys);
        PasswordUI ui = passwordUI.GetComponent<PasswordUI>();
        ui.SetContent(cubeList.IndexOf(cubeName), passwordDictionary[cubeName]);
    }
}
