﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Cube : NetworkBehaviour
{
    string itemType;
    [SyncVar]
    public string item;

    [Client]
    public string RetrieveItem()
    {
        PropSetting setting = GetComponentInParent<PropSetting>();
        ItemPopup popup = setting.itemPopup.GetComponent<ItemPopup>();
        popup.SetContent(setting.propDictionary, item);
        return item;
    }
}
