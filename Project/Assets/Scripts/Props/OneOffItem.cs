﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OneOffItem : MonoBehaviour
{
    string itemType;
    Color32 emptyImage = new Color32(60, 56, 54, 255);

    void GetItem()
    {
        GameObject itemObject = transform.Find("Mask/Image").gameObject;
        Image itemImage = itemObject.GetComponent<Image>();
        itemType = null;
        itemImage.sprite = null;
        itemImage.color = emptyImage;
    }

    public void SetItem(string item, Sprite sprite)
    {
        GameObject itemObject = transform.Find("Mask/Image").gameObject;
        Image itemImage = itemObject.GetComponent<Image>();
        if (sprite)
        {
            itemType = item;
            itemImage.sprite = sprite;
        }
        else itemImage.color = emptyImage;
    }

    public void UseItem(PlayerController player)
    {
        switch (itemType)
        {
            case "First Aid Kit":
                GetItem();
                player.CmdHealPlayer(-1);
                break;
            case "Flashlight":
                break;
            case "Map":
                GameObject mapPopup = GetComponentInParent<ItemUI>().mapPopup.gameObject;
                ToggleableUI popup = mapPopup.GetComponent<ToggleableUI>();
                popup.ToggleActivation();
                break;
            case "Molotov Cocktail":
                GetItem();
                player.ThrowMolotovCocktail();
                break;
            default: break;
        }
    }
}
