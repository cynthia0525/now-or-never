﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public const int INIT_STR = 50;
    bool attack = false;

    int str = INIT_STR;
    public int durability
    {
        get { return str; }
        set { str = value; }
    }
    public WeaponDurability durabilityUI;

    [Header("[Setting]")]
    [SerializeField]
    int atk = 0;
    public int attackPoint
    {
        get { return atk; }
    }

    [SerializeField]
    int def = 0;
    public int defensePoint
    {
        get { return def; }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (attack) Attack(collider.gameObject);
    }

    public void ToggleActive()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public void ToggleAttack()
    {
        attack = !attack;
    }

    protected void Attack(GameObject target)
    {
        switch (target.tag)
        {
            case "Player":
                PlayerController player = target.GetComponent<PlayerController>();
                player.ChangeHealth(false, attackPoint);
                break;
            case "Enemy":
                EnemyController enemy = target.GetComponent<EnemyController>();
                enemy.BeingAttacked(attackPoint);
                break;
            case "Weapon":
                Weapon weapon = target.GetComponent<Weapon>();
                weapon.durability--;
                break;
            default:
                break;
        }
        durability--;
    }
}
