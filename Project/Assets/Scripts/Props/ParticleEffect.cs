﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ParticleEffect : NetworkBehaviour
{
    [Header("[Setting]")]
    [SerializeField]
    float attack = 0;

    void OnParticleCollision(GameObject collider)
    {
        if (!isServer) return;

        if (collider.CompareTag("Player"))
        {
            PlayerController player = collider.GetComponent<PlayerController>();
            player.ChangeHealth(false, attack);
        }
        else if (collider.CompareTag("Enemy"))
        {
            EnemyController enemy = collider.GetComponent<EnemyController>();
            enemy.BeingAttacked(attack);
        }
    }
}
