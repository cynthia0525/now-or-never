﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;

public class MatchMakingManager : NetworkLobbyManager
{
    public static MatchMakingManager instance { get; private set; }

    public bool isHost;
    MatchInfo roomInfo;
    List<GameObject> roomList = new List<GameObject> { };

    Transform roomLayout;
    [SerializeField]
    GameObject roomButton;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        StartManager();
    }

    public void StartManager()
    {
        if (matchMaker == null) StartMatchMaker();
        roomLayout = GameObject.FindGameObjectWithTag("Lobby").transform; // missing reference on reload
        ListRoom();
    }

    public void CreateRoom(string roomName, uint roomSize)
    {
        RoomManager.instance.LoadRoom(roomName);
        if (roomSize == 1)
        {
            StartHost();
            RoomManager.instance.EnterRoom();
        }
        else matchMaker.CreateMatch(roomName, roomSize, true, "", "", "", 0, 0, OnMatchCreate);
    }

    public override void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            roomInfo = matchInfo;
            StartHost(matchInfo);
            RoomManager.instance.EnterRoom();
        }
        else
        {
            RoomManager.instance.LoadRoom();
            ErrorMessage.instance.ThrowError("Fail to create the room");
        }
    }

    public void JoinRoom(MatchInfoSnapshot room)
    {
        RoomManager.instance.LoadRoom(room.name);
        matchMaker.JoinMatch(room.networkId, "", "", "", 0, 0, OnMatchJoined);
    }

    public override void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            StartClient(matchInfo);
            RoomManager.instance.EnterRoom();
        }
        else
        {
            RoomManager.instance.LoadRoom();
            ErrorMessage.instance.ThrowError("Fail to join the room");
        }
    }

    public void ListRoom()
    {
        matchMaker.ListMatches(0, 20, "", false, 0, 0, OnMatchList);
    }

    public override void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        if (success)
        {
            ClearRoom();
            foreach (MatchInfoSnapshot match in matches)
            {
                GameObject roomObject = Instantiate(roomButton);
                roomObject.transform.SetParent(roomLayout, false);
                LobbyManager room = roomObject.GetComponent<LobbyManager>();
                if (room != null) room.SetRoom(match);
                roomList.Add(roomObject);
            }
        }
        else ErrorMessage.instance.ThrowError("Fail to connect to match maker");
    }

    public void LeaveRoom()
    {
        if (isHost)
        {
            StopHost();
            PlayerManager.instance.playerDictionary.Clear();
            if (RoomManager.instance != null) RoomManager.instance.playerDictionary.Clear();
        }
        else StopClient();
    }

    void ClearRoom()
    {
        foreach (GameObject room in roomList) Destroy(room);
        roomList.Clear();
    }


    string currentScene { get { return SceneManager.GetActiveScene().name; } }

    // callback on clients when the server disconnects
    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        ErrorMessage.instance.ThrowError("Host has diconnected");
        if (currentScene == lobbyScene) RoomManager.instance.ExitRoom(true);
    }

    // callback on the server when a client disconnects
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        List<NetworkInstanceId> networkedObjects = new List<NetworkInstanceId>(conn.clientOwnedObjects);
        base.OnServerDisconnect(conn);

        // remove player status
        if (currentScene == lobbyScene)
        {
            PlayerManager.instance.playerDictionary.Remove(networkedObjects[0]);
            RoomManager.instance.playerDictionary.Remove(networkedObjects[0]);
            CheckReadyToBegin();
        }
        // disconnected client has not ended his game
        else if (networkedObjects.Count > 1)
            GameObject.Find("Timer/Container/Text").GetComponent<Timer>().OnDisconnect(networkedObjects[1]);
    }

    // callback on the server when all players are ready
    public override void OnLobbyServerPlayersReady()
    {
        GameObject[] lobbyPlayers = GameObject.FindGameObjectsWithTag("LobbyPlayer");
        foreach (GameObject lobbyPlayer in lobbyPlayers)
            lobbyPlayer.GetComponent<LobbyPlayer>().RpcReadyToStart();
        base.OnLobbyServerPlayersReady();
        PlayerManager.instance.playerDictionary.Clear();

        // prevent new players from joining the started match
        matchMaker.SetMatchAttributes(roomInfo.networkId, false, 0, OnSetMatchAttributes);
    }

    //public override void OnStopClient()
    //{
    //    if (currentScene != lobbyScene) Destroy(gameObject);
    //}
}
