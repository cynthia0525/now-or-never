﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance { get; private set; }

    public string playerName { get; private set; }
    public Dictionary<NetworkInstanceId, string> playerDictionary = new Dictionary<NetworkInstanceId, string>(); // only used in Server

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void InitPlayerName(string name)
    {
        playerName = name;
    }
}
