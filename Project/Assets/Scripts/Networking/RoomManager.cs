﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour
{
    public static RoomManager instance { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        if (MatchMakingManager.instance != null)
            MatchMakingManager.instance.StartManager();
        gameObject.SetActive(false);
        status.SetActive(false);
    }

    // middleware of ListRoom to avoid missing reference on reload
    public void ListRoom()
    {
        MatchMakingManager.instance.ListRoom();
    }

    string roomName;
    public LobbyPlayer localPlayer; // only used in LocalPlayer
    public Dictionary<NetworkInstanceId, bool> playerDictionary = new Dictionary<NetworkInstanceId, bool>(); // only used in Server
    public Transform playerLayout;

    [SerializeField]
    Text heading;
    [SerializeField]
    GameObject status;
    [SerializeField]
    Text label;
    [SerializeField]
    GameObject roomList;

    public void OnToggleReady(bool state)
    {
        label.text = state ? "Cancel" : "Ready";
    }

    public void ToggleReady()
    {
        if (localPlayer.readyToBegin) localPlayer.SendNotReadyToBeginMessage();
        else localPlayer.SendReadyToBeginMessage();
    }

    public void StartMatch()
    {
        gameObject.SetActive(false);
        status.SetActive(true);
    }

    public void LoadRoom(string roomName = null)
    {
        bool failedToLoad = string.IsNullOrEmpty(roomName);
        this.roomName = roomName;
        roomList.SetActive(failedToLoad);
        status.SetActive(!failedToLoad);
    }

    public void EnterRoom()
    {
        status.SetActive(false);
        heading.text = roomName;
        label.text = "Ready";
        gameObject.SetActive(true);
    }

    public void ExitRoom(bool forceLeave)
    {
        gameObject.SetActive(false);
        heading.text = "Lobby";
        roomList.SetActive(true);

        if (!forceLeave) MatchMakingManager.instance.LeaveRoom();

        // restart the match making manager for unexpected disconnection
        MatchMakingManager.instance.StartManager();
    }
}
