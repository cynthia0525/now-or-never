﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyPlayer : NetworkLobbyPlayer
{
    [SerializeField]
    public SpriteDictionary statusDictionary;

    void Start()
    {
        transform.SetParent(RoomManager.instance.playerLayout, false);

        if (!isLocalPlayer) return;
        CmdUpdateStatus(PlayerManager.instance.playerName);
    }

    [Command]
    void CmdUpdateStatus(string name)
    {
        // update the status of existing players
        foreach (NetworkLobbyPlayer player in FindObjectsOfType<NetworkLobbyPlayer>())
        {
            NetworkInstanceId targetId = player.netId;
            if (RoomManager.instance.playerDictionary.ContainsKey(targetId))
                RpcUpdateStatus(targetId, PlayerManager.instance.playerDictionary[targetId], RoomManager.instance.playerDictionary[targetId]);
        }

        // add status of the new player
        PlayerManager.instance.playerDictionary.Add(netId, name);
        RoomManager.instance.playerDictionary.Add(netId, readyToBegin);
        RpcUpdateStatus(netId, name, readyToBegin);
    }

    [ClientRpc]
    void RpcUpdateStatus(NetworkInstanceId targetId, string name, bool state)
    {
        if (isLocalPlayer || targetId == netId)
        {
            GameObject player = targetId == netId ? gameObject : ClientScene.FindLocalObject(targetId);
            player.GetComponent<Image>().sprite = statusDictionary[state.ToString()];
            player.GetComponentInChildren<Text>().text = name;
        }
    }


    // make the lobby player DDOL so as to be successfully replaced by the game plater
    [ClientRpc]
    public void RpcReadyToStart()
    {
        transform.SetParent(null);
        DontDestroyOnLoad(gameObject);
        if (isLocalPlayer) RoomManager.instance.StartMatch();
    }


    public override void OnStartLocalPlayer()
    {
        MatchMakingManager.instance.isHost = isServer;
        RoomManager.instance.localPlayer = this;
    }

    public override void OnClientReady(bool readyState)
    {
        if (isLocalPlayer) RoomManager.instance.OnToggleReady(readyState);
        GetComponent<Image>().sprite = statusDictionary[readyState.ToString()];
        if (isServer) RoomManager.instance.playerDictionary[netId] = readyState;
    }
}
