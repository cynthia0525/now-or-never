﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class LobbyManager : MonoBehaviour
{
    MatchInfoSnapshot match;

    public void SetRoom(MatchInfoSnapshot match)
    {
        this.match = match;
        Text roomName = GetComponentInChildren<Text>();
        roomName.text = string.Format("{0} ({1}/{2})", match.name, match.currentSize, match.maxSize);
    }

    public void JoinRoom()
    {
        MatchMakingManager.instance.JoinRoom(match);
    }
}
