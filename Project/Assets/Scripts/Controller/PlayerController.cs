﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
    const int MAX_HEALTH = 100;

    Rigidbody2D rigidBody;
    Animator animator;
    AudioSource audioSource;
    bool directionX = false; // false -> left(-ve); true -> right(+ve)
    public bool blockInput = false;
    LayerMask objectMask;
    LayerMask enemyMask;

    float hp = MAX_HEALTH;
    bool attack = false;
    List<Weapon> weaponList = new List<Weapon> { };
    Weapon[] weapons;
    GameObject headerObject;
    OneOffItem[] itemList;
    [SerializeField]
    GameObject flames = null;
    [SerializeField]
    AudioDictionary audioDictionary;

    [Header("[Setting]")]
    [SerializeField]
    float moveSpeed = 0;
    [SerializeField]
    float selectArea = 0;
    [SerializeField]
    float targetArea = 0;
    [SerializeField]
    float attackDistance = 0;

    // Start is called before the first frame update
    void Start()
    {
        // avoid searching during runtime
        weapons = GetComponentsInChildren<Weapon>(true);
        headerObject = transform.Find("Body/Head/Header").gameObject;
        audioSource = GetComponent<AudioSource>();
        if (!isLocalPlayer) return;

        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        objectMask = LayerMask.GetMask("SelectableObject");
        enemyMask = LayerMask.GetMask("Enemy");
        itemList = FindObjectOfType<ItemUI>().items;

        // add player status
        CmdAddPlayerStatus(PlayerManager.instance.playerName);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;
    }

    void FixedUpdate()
    {
        if (!isLocalPlayer) return;
        if (!blockInput) HandleUserInput();
    }

    void HandleUserInput()
    {
        // complete "Attack" / "Die" animation first
        AnimatorStateInfo state = animator.GetCurrentAnimatorStateInfo(0);
        if (state.IsName("Attack") || state.IsName("Die")) return;

        // switch to normal mode
        if (attack)
        {
            attack = false;
            CmdToggleAttack(true);
        }

        // toggle key guide for item selection
        if (Input.GetButtonDown("ItemSelector") || Input.GetButtonUp("ItemSelector"))
        {
            GameObject itemKeys = itemList[0].GetComponentInParent<ItemUI>().itemKeys.gameObject;
            ToggleableUI keyGuide = itemKeys.GetComponent<ToggleableUI>();
            keyGuide.ToggleActivation();
            return;
        }

        // use one-off item
        if (Input.GetButton("ItemSelector"))
        {
            if (Input.GetButtonDown("Item1"))
                itemList[0].UseItem(this);
            else if (Input.GetButtonDown("Item2"))
                itemList[1].UseItem(this);
            //else if (Input.GetButtonDown("Item3"))
            //    itemList[2].UseItem(this);
            //else if (Input.GetButtonDown("Item4"))
            //    itemList[3].UseItem(this);
            return;
        }

        // handle select event
        if (Input.GetButtonDown("Select"))
        {
            Select();
        }

        // perform "Sit" animation
        if (Input.GetButton("Select") || Input.GetButton("Crouch"))
        {
            animator.Play("Sit");
            return;
        }

        // perform "Attack" animation
        if (Input.GetButton("Attack"))
        {
            CmdToggleAttack(false);
            attack = true;
            PerformAction("Attack");
            return;
        }

        // get input movement
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        // perform "Idle" animation
        if (horizontal == 0 && vertical == 0)
        {
            animator.Play("Idle");
            return;
        };

        // perform "Run" animation
        Move(horizontal, vertical);
    }

    void Select()
    {
        // find the selected object within the selected area
        Collider2D collider = Physics2D.OverlapCircle(rigidBody.position, selectArea, objectMask);
        if (collider == null) return;

        // enter the emigration hall
        if (collider.name == "Emigration Hall")
        {
            GameObject passwordPopup = collider.gameObject.transform.Find("PasswordPopup").gameObject;
            PasswordPopup popup = passwordPopup.GetComponentInChildren<PasswordPopup>();
            popup.OpenPopup();
            blockInput = true;
        }

        // get the item from the selected cube
        else
        {
            GameObject cubeObject = collider.gameObject;
            Cube cube = cubeObject.GetComponent<Cube>();
            string item = cube.RetrieveItem();
            if (!string.IsNullOrEmpty(item))
            {
                // empty the cube on server
                CmdGetItem(cubeObject);

                if (item == "Scroll") CmdGetPassword(cubeObject);
                else CmdGetWeapon(item);
            }
        }
    }

    void Move(float horizontal, float vertical)
    {
        // flip the character if opposite direction
        if (horizontal != 0 && directionX != horizontal > 0)
        {
            directionX = !directionX;
            CmdFlipPlayer();
        }

        // translate the character
        Vector2 move = new Vector2(horizontal, vertical);
        Vector2 position = rigidBody.position + move * 0.1f;
        rigidBody.MovePosition(position);
        PerformAction("Run");
    }

    void PerformAction(string action)
    {
        animator.Play(action);
        CmdPlaySoundEffect(action);
    }

    [Server]
    public void ChangeHealth(bool mode, float value = 0)
    {
        // mode: true -> healing; false -> attacking
        if (mode)
        {
            hp = hp + value;
            if (hp > MAX_HEALTH) hp = MAX_HEALTH;
        }
        else
        {
            int def = 0;
            foreach (Weapon weapon in weaponList.ToArray())
            {
                def += weapon.defensePoint;
                RemoveBrokenWeapon(weapon);
            }
            def /= 2;
            hp = value > def ? hp + def - value : hp;
        }

        RpcChangeHealth(mode, hp);
    }

    [Client]
    public void ThrowMolotovCocktail()
    {
        PerformAction("Attack");
        Invoke("BurnWithUs", animator.GetCurrentAnimatorStateInfo(0).length);
    }

    void BurnWithUs()
    {
        // check if there is any enemy nearby
        Collider2D collider = Physics2D.OverlapCircle(rigidBody.position, targetArea, enemyMask);
        Vector3 target;
        if (collider == null)
        {
            // randomly pick a point
            Vector2 point = Random.insideUnitCircle;
            point.x = Mathf.Abs(point.x);
            if (!directionX) point.x *= -1;
            target = new Vector3(point.x, point.y, 0);
        }
        else
        {
            // target at the enemy
            Vector3 enemy = collider.transform.position - transform.position;
            target = enemy.normalized;
        }

        // simulate throwing the patrol bomb
        target *= attackDistance;
        target.x += rigidBody.position.x;
        target.y += rigidBody.position.y;

        CmdBurnWithUs(target);
    }

    public override void OnStartLocalPlayer()
    {
        // highlight the color of local player
        Component[] sprites = GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer sprite in sprites)
            sprite.material.color = Color.yellow;

        // initialize the position to the safety house
        GameObject safetyHouse = GameObject.FindGameObjectWithTag("SafetyHouse");
        transform.position = safetyHouse.transform.position + new Vector3(-0.5f, -3f, 0f);

        // set the target of cameras
        Camera.main.GetComponent<CameraController>().SetTarget(transform);
        GameObject.Find("Map Camera").GetComponent<CameraController>().SetTarget(transform);

        // set the target of password popup
        Resources.FindObjectsOfTypeAll<PasswordPopup>()[0].SetTarget(gameObject);
    }


    [Command]
    void CmdAddPlayerStatus(string name)
    {
        PlayerStatus playerStatus = GameObject.Find("PlayerStatus").GetComponent<PlayerStatus>();

        KeyValuePair<NetworkInstanceId, string> newPlayer = new KeyValuePair<NetworkInstanceId, string>(netId, "Alive");
        // add existing players to the new player
        foreach (KeyValuePair<NetworkInstanceId, string> player in playerStatus.playerDictionary)
            RpcAddPlayerStatus(player.Key.ToString(), PlayerManager.instance.playerDictionary[player.Key], player.Value);
        // add new player to the existing players
        RpcAddPlayerStatus(newPlayer.Key.ToString(), name, newPlayer.Value);

        // update player list and UI on server
        PlayerManager.instance.playerDictionary.Add(newPlayer.Key, name);
        playerStatus.playerDictionary.Add(newPlayer.Key, newPlayer.Value);
        if (!isClient) playerStatus.AddPlayer(newPlayer.Key.ToString(), name, newPlayer.Value);
    }

    /* netId is always the newly connected player object
     * 
     * 1. isLocalPlayer && targetId != netId.ToString()
     * => add existing players to the new player
     * 
     * 2. !isLocalPlayer && targetId == netId.ToString()
     * => add new player to the existing players
     */

    [ClientRpc]
    void RpcAddPlayerStatus(string targetId, string name, string status)
    {
        if (isLocalPlayer && targetId != netId.ToString() || !isLocalPlayer && targetId == netId.ToString())
            GameObject.Find("PlayerStatus").GetComponent<PlayerStatus>().AddPlayer(targetId, name, status);
    }

    [ClientRpc]
    void RpcChangePlayerStatus(string targetId, string status)
    {
        if (!isLocalPlayer)
            GameObject.Find("PlayerStatus").GetComponent<PlayerStatus>().ChangeStatus(targetId, status);
    }


    [Command]
    void CmdPlaySoundEffect(string action)
    {
        RpcPlaySoundEffect(action);
    }

    [ClientRpc]
    void RpcPlaySoundEffect(string action)
    {
        if (action == "Run" && audioSource.isPlaying) return;
        audioSource.PlayOneShot(audioDictionary[action]);
    }


    void FlipPlayer()
    {
        transform.localScale = transform.localScale * new Vector2(-1, 1);
    }

    [Command]
    void CmdFlipPlayer()
    {
        FlipPlayer();
        RpcFlipPlayer();
    }

    [ClientRpc]
    void RpcFlipPlayer()
    {
        if (!isServer) FlipPlayer();
    }


    [Command]
    void CmdGetItem(GameObject cubeObject)
    {
        Cube cube = cubeObject.GetComponent<Cube>();
        cube.item = null;
    }

    [Command]
    void CmdGetPassword(GameObject cubeObject)
    {
        PropSetting setting = cubeObject.GetComponentInParent<PropSetting>();
        setting.GetPassword(cubeObject.name);
    }

    [Command]
    void CmdGetWeapon(string item)
    {
        foreach (Weapon weapon in weapons)
            if (weapon.name == item)
            {
                if (weaponList.Contains(weapon))
                    weapon.durability += Weapon.INIT_STR;
                else
                {
                    weapon.durability = Weapon.INIT_STR;
                    weaponList.Add(weapon);
                    if (!isClient) weapon.ToggleActive();
                    RpcToggleWeapon(weapon.name, true);
                }
                RpcUpdateWeaponUI(weapon.name, weapon.durability);
            }
    }

    void RemoveBrokenWeapon(Weapon weapon)
    {
        RpcUpdateWeaponUI(weapon.name, weapon.durability);

        bool broken = weapon.durability <= 0;
        if (broken)
        {
            weaponList.Remove(weapon);
            if (!isClient) weapon.ToggleActive();
            RpcToggleWeapon(weapon.name, true);
        }
    }

    [ClientRpc]
    void RpcToggleWeapon(string weaponName, bool toggleUI)
    {
        foreach (Weapon weapon in weapons)
            if (weapon.name == weaponName)
            {
                weapon.ToggleActive();
                if (isLocalPlayer && toggleUI)
                    weapon.durabilityUI = weapon.durabilityUI ? PlayerHP.instance.RemoveWeapon(weapon.name) : PlayerHP.instance.AddWeapon(weapon.name);
            }
    }

    [Command]
    void CmdToggleAttack(bool checkDurability)
    {
        if (weaponList.Count > 0)
            foreach (Weapon weapon in weaponList.ToArray())
            {
                if (checkDurability) RemoveBrokenWeapon(weapon);
                weapon.ToggleAttack();
            }
        else
        {
            Weapon header = headerObject.GetComponent<Weapon>();
            if (!isClient) header.ToggleActive();
            header.ToggleAttack();
            RpcToggleWeapon(headerObject.name, false);
        }
    }


    [Command]
    void CmdBurnWithUs(Vector3 target)
    {
        GameObject spawn = Instantiate(flames, target, Quaternion.identity);
        NetworkServer.Spawn(spawn);
    }

    [Command]
    public void CmdHealPlayer(float value)
    {
        ChangeHealth(true, value < 0 ? MAX_HEALTH : value);
    }

    [Command]
    public void CmdDestroy(float time, string status)
    {
        Destroy(gameObject, time);

        PlayerStatus playerStatus = GameObject.Find("PlayerStatus").GetComponent<PlayerStatus>();

        // update player status UI
        string targetId = netId.ToString();
        if (!isClient) playerStatus.ChangeStatus(targetId, status);
        playerStatus.playerDictionary[netId] = status;
        RpcChangePlayerStatus(targetId, status);

        // cancel updating timer when no one is alive
        if (!playerStatus.playerDictionary.ContainsValue("Alive"))
        {
            GameObject.Find("Timer/Container/Text").GetComponent<Timer>().CancelInvoke("UpdateTimer");
            if (!isClient) BackgroundMusicController.instance.ChangeScene("Landing");
        }
    }

    [ClientRpc]
    void RpcChangeHealth(bool playSound, float hp)
    {
        if (isLocalPlayer && playSound) audioSource.PlayOneShot(audioDictionary["Recover"]);

        this.hp = hp;

        if (isLocalPlayer)
        {
            // update HP UI
            PlayerHP.instance.SetValue(hp / MAX_HEALTH);

            // check if player is dead
            if (hp <= 0)
            {
                PerformAction("Die");
                CmdDestroy(animator.GetCurrentAnimatorStateInfo(0).length, "Dead");
                BackgroundMusicController.instance.ChangeScene("GameOver");
            }
        }
    }


    [ClientRpc]
    public void RpcUpdateWeaponUI(string weaponName, int durability)
    {
        if (isLocalPlayer)
            foreach (Weapon weapon in weapons)
                if (weapon.name == weaponName)
                    weapon.durabilityUI.UpdateUI(durability);
    }
}
