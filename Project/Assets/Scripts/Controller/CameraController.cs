﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Transform player;

    [Header("[Setting]")]
    [SerializeField]
    int depth = 0;

    public void SetTarget(Transform target)
    {
        player = target;
    }

    // Start is called before the first frame update
    void Start()
    {
        GameObject safetyHouse = GameObject.FindGameObjectWithTag("SafetyHouse");
        transform.position = safetyHouse.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
            transform.position = player.position + new Vector3(0, 0, depth);
    }
}
