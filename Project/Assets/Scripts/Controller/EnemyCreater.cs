﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemyCreater : NetworkBehaviour
{
    [SerializeField]
    GameObject enemy = null;
    [SerializeField]
    List<Vector2> initialLocation = null;
    int num = 5;

    // Start is called before the first frame update
    void Start()
    {
        if (!isServer) return;
        List<int> renderedPosition = new List<int> { };
        do
        {
            int index = Random.Range(0, initialLocation.Count);
            if (!renderedPosition.Contains(index))
            {
                renderedPosition.Add(index);
                GameObject spawn = Instantiate(enemy, initialLocation[index], Quaternion.identity);
                NetworkServer.Spawn(spawn);
            }
        } while (renderedPosition.Count < num);
    }
}
