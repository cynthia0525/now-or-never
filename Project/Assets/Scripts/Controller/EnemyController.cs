﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemyController : NetworkBehaviour
{
    const int MAX_HEALTH = 100;

    Rigidbody2D rigidBody;
    Animator animator;
    AudioSource audioSource;
    bool directionX = true; // false -> left(-ve); true -> right(+ve)
    [SyncVar(hook = "OnChangeScale")]
    Vector2 localScale;
    LayerMask playerMask;
    EnemyHP enemyHP;

    [SyncVar(hook = "OnChangeHealth")]
    float hp = MAX_HEALTH;

    // patrolling variables
    Vector2 basePosition, start, destination;
    float currentDistance = 0, totalDistance = 0;

    // attacking variables
    bool attack = false;
    bool cooldown = false;
    string weapon;
    [SerializeField]
    GameObject grenades = null;
    [SerializeField]
    AudioDictionary audioDictionary;

    [Header("[Setting]")]
    [SerializeField]
    float walkSpeed = 0;
    [SerializeField]
    float runSpeed = 0;
    [SerializeField]
    float patrolArea = 0;
    [SerializeField]
    float targetArea = 0;
    [SerializeField]
    float attackDistance = 0;
    [SerializeField]
    float meleeCooldownTime = 0;
    [SerializeField]
    float rangedCooldownTime = 0;
    [SerializeField]
    string[] weapons = { };

    // Start is called before the first frame update
    void Start()
    {
        enemyHP = GetComponentInChildren<EnemyHP>();
        audioSource = GetComponent<AudioSource>();
        if (!isServer) return;

        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        localScale = transform.localScale;
        playerMask = LayerMask.GetMask("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (!isServer) return;
    }

    void FixedUpdate()
    {
        if (!isServer) return;

        // complete "Attack" / "Die" animation first
        AnimatorStateInfo state = animator.GetCurrentAnimatorStateInfo(0);
        if (state.IsName("Attack 1") || state.IsName("Attack 2") || state.IsName("Attack 3") ||
            state.IsName("Hit") || state.IsName("Die")) return;

        // deactive all weapons after attack
        if (attack)
        {
            attack = false;
            ToggleWeapon(weapon);
            RpcToggleWeapon(weapon);
        }

        // check if there is any player nearby
        Collider2D collider = Physics2D.OverlapCircle(rigidBody.position, targetArea, playerMask);
        if (collider == null)
        {
            if (totalDistance == 0)
            {
                basePosition = transform.position;
                PickNewPatrolPoint();
            }
            Patrol();
        }
        else
        {
            Rigidbody2D target = collider.gameObject.GetComponent<Rigidbody2D>();
            Chase(target);
            totalDistance = 0;
        }
    }

    void PickNewPatrolPoint()
    {
        start = transform.position;
        destination = basePosition + Random.insideUnitCircle * patrolArea;
        currentDistance = 0;
        totalDistance = Vector2.Distance(start, destination);
    }

    void Patrol()
    {
        if (currentDistance >= totalDistance) PickNewPatrolPoint();
        else currentDistance += walkSpeed;
        Move(destination, walkSpeed);
        PerformAction("Walk", false);
    }

    void Chase(Rigidbody2D target)
    {
        if (Vector2.Distance(target.position, rigidBody.position) > attackDistance)
        {
            Move(target.position, runSpeed);
            PerformAction("Run", false);
        }
        else Attack(target.position);
    }

    void Move(Vector2 target, float moveSpeed)
    {
        // find the direction to the target
        Vector2 direction = target - rigidBody.position;
        float speed = Mathf.Min(moveSpeed, direction.magnitude);
        direction = direction.normalized;

        // flip the character if opposite direction
        if (direction.x != 0 && directionX != direction.x > 0)
        {
            directionX = !directionX;
            localScale = localScale * new Vector2(-1, 1);
            RpcAvoidFlipping(new Vector2(directionX ? 1 : -1, 1));
        }

        // translate the character
        Vector2 position = rigidBody.position + direction * speed;
        rigidBody.MovePosition(position);
    }

    void Attack(Vector2 target)
    {
        animator.Play("Idle");
        int index = Random.Range(0, weapons.Length);
        weapon = weapons[index];
        if (!cooldown || weapon == "Punch")
        {
            // activate all weapons before attack
            RpcToggleWeapon(weapon);
            ToggleWeapon(weapon);
            attack = true;
            PerformAction(weapon, true);

            // create smoke effect for ranged weapon
            if (weapon == "Ranged Weapon")
            {
                GameObject spawn = Instantiate(grenades, target, Quaternion.identity);
                NetworkServer.Spawn(spawn);
            }

            // set cooldown for the next special attack
            if (weapon != "Punch")
            {
                cooldown = true;
                float cooldownTime = weapon == "Melee Weapon" ? meleeCooldownTime : rangedCooldownTime;
                Invoke("Cooldown", cooldownTime);
            }
        }
    }

    void Cooldown()
    {
        cooldown = false;
    }

    void PerformAction(string action, bool trigger)
    {
        if (trigger) animator.SetTrigger(action);
        else animator.Play(action);
        RpcPlaySoundEffect(action);
    }

    [Server]
    public void BeingAttacked(float damage)
    {
        hp -= damage;

        if (hp <= 0)
        {
            PerformAction("Die", false);
            Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).length);
        }
    }

    [ClientRpc]
    void RpcPlaySoundEffect(string action)
    {
        if ((action == "Walk" || action == "Run") && audioSource.isPlaying) return;
        audioSource.PlayOneShot(audioDictionary[action]);
    }

    void OnChangeScale(Vector2 scale)
    {
        transform.localScale = scale;
    }

    [ClientRpc]
    void RpcAvoidFlipping(Vector2 flipX)
    {
        enemyHP.AvoidFlipping(flipX);
    }

    void OnChangeHealth(float hp)
    {
        enemyHP.SetValue(hp / MAX_HEALTH);
    }

    void ToggleWeapon(string weaponName)
    {
        foreach (string side in new List<string> { "Left", "Right" })
        {
            Transform weaponTransform = transform.Find(string.Format("Pelvis/Torso/{0} Shoulder/{0} Elbow/{0} Wrist/{1}", side, weaponName));
            if (weaponTransform)
            {
                Weapon weapon = weaponTransform.gameObject.GetComponent<Weapon>();
                weapon.ToggleActive();
                if (isServer) weapon.ToggleAttack();
            }
        }
    }

    [ClientRpc]
    void RpcToggleWeapon(string weaponName)
    {
        if (!isServer) ToggleWeapon(weaponName);
    }
}
