﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackgroundMusicController : MonoBehaviour
{
    public static BackgroundMusicController instance { get; private set; }
    string nextScene = null;

    void Awake()
    {
        instance = this;
    }

    public void ChangeScene(string scene, float duration = 0.5f)
    {
        nextScene = scene;
        AudioSource bgm = GetComponent<AudioSource>();
        Invoke("LoadScene", duration);
        StartCoroutine(AudioController.FadeOut(bgm, duration));
    }

    void LoadScene()
    {
        if (nextScene == "GameClear" || nextScene == "GameOver")
            StartCoroutine(LoadSceneAdditive());
        else SceneManager.LoadScene(nextScene);
    }

    IEnumerator LoadSceneAdditive()
    {
        // mute all the sound effects in current scene
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        foreach (AudioSource audioSource in audioSources)
            audioSource.mute = true;

        // close all the popups in current scene
        GameObject[] popups = GameObject.FindGameObjectsWithTag("Popup");
        foreach (GameObject popup in popups)
            popup.SetActive(false);

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(nextScene, LoadSceneMode.Additive);

        // wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone) yield return null;

        SceneManager.SetActiveScene(SceneManager.GetSceneByName(nextScene));
    }
}
